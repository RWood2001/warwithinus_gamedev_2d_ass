using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_AnimaitonController : MonoBehaviour
{
    private Animator anim;

    //reference to the players movement script
    [SerializeField]private NewCharactermovement movement;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();

        movement = GetComponent<NewCharactermovement>();
    }

    // Update is called once per frame
    void Update()
    {
        MovementParameter();   
    }

    void MovementParameter ()
    {
        int abosluteValue = Mathf.Abs((int)movement.horizontalValue);

        anim.SetInteger("Movement", abosluteValue);
    }

    void AttackA()
    {
        //Fires the trigger for the attack animation
        anim.SetTrigger("AttackA"); 
    }
}
