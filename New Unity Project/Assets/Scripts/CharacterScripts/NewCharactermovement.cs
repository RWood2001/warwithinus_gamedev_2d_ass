using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewCharactermovement : MonoBehaviour
{
    //Variable that references the control scheme
    private scp_ControlScheme controls;

    //reference to the rigidbody 
    private Rigidbody2D rb;

    //will store the direction of the button press 
    public float horizontalValue;

    //Variable to control how fast the player wil move 
    public float speed;

    //Variable to deal with the flip 
    private Vector2 flippedscale = new Vector2(-1f, 1);

    //
    private Player_AnimaitonController animController;

    public Animator animVariable;

    public Animator anim;

    public int AttackA;

    private void Awake()
    {
        //Create a new object of type control scheme
        controls = new scp_ControlScheme();

        //will fire that method if we press left or right
        controls.Player.HorizontalMovement.performed += _ => OnHorizontalMovement();
        controls.Player.HorizontalMovement.canceled += _ => rb.velocity = Vector2.zero;

        //reference to the dash 
        controls.Player.Dash.performed += _ => OnDash();
        //Reference to the Attack
        controls.Player.Attack.performed += _ => OnAttack();
        //Reference to the jump
        controls.Player.Jump.performed += _ => OnJump();

        //Finds the animation controller and stores it into the variable 
        animVariable = GetComponent<Player_AnimaitonController>();


    }

    private void OnEnable()
    {
        //Enables the controls script
        controls.Enable();
    }

    private void OnDisable()
    {
        //Disables the control script
        controls.Disable();
    }


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if (rb == null)
        {
            Debug.LogError("rb is null");
        }
    }

    // Update is called once per frame
    void Update()
    {
        StoreHorizontalValue();
        MovePlayer();
    }

    void OnHorizontalMovement()
    {
        FlipThePlayer();
    }

    void OnDash()
    {

    }
    void OnAttack()
    {
        animController.AttackA();
    }

    void OnJump()
    {
        Debug.Log("Jump Pressed");
    }

    void StoreHorizontalValue()
    {
        horizontalValue = controls.Player.HorizontalMovement.ReadValue<float>();
    }

    void MovePlayer()
    {
        Vector2 force = new Vector2(horizontalValue * speed * Time.deltaTime, 0);

        rb.AddForce(force, ForceMode2D.Impulse);
    }

    void FlipThePlayer()
    {
        if (controls.Player.HorizontalMovement.ReadValue<float>() == -1)
        {
            transform.localScale = flippedscale;

        }
        else if (controls.Player.HorizontalMovement.ReadValue<float>() == 1)
        {
            transform.localScale = Vector2.one;
        }
    }
}
