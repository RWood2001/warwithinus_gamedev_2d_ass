using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Movement : MonoBehaviour
{
    //Variable to store the target of the camera
    public Transform targets;

    //Variable is to store the offset of the camera 
    public Vector3 cameraOffset = new Vector3(0, 0, -10);

    //Variable that will be used by the lerp 
    public float smoothSpeed = 0.125f;
    
    private void FixedUpdate()
    {
        CameraFollow(); 
    }

    void CameraFollow()
    {
        Vector3 desiredPosition = targets.position + cameraOffset;
        //will lerp the position
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);

        //Will follow the player
        transform.position = targets.position + cameraOffset;
    }

}
